package plt;

public class Translator {
	private String phrase;
	public static final  String NIL = "nil";
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() throws TranslatorException{
		String phraseTemp = "";
		String[] words = phrase.split("\\s+");
		
		for(int i = 0; i < words.length; i++) {
			phraseTemp = phraseTemp + " " + translateWord(words[i]);
		}
		phrase = phraseTemp.trim();
		return phrase;
	}

	private String translateWord(String word) throws TranslatorException{
		String[] minusWords = word.split("\\-+");
		boolean wasUpperCase = isUpperCase(word);
		boolean wasTitleCase = isTitleCase(word);
		boolean wasLowerCase = isLowerCase(word);
		
		word = word.toLowerCase();
		if(minusWords.length > 1) {
			word = translateMinusWord(minusWords);
		}else {
			String initialPunctuation = "";
		    String finalPunctuation = "";
		
			if(startsWithPuntuation(word)) {
				initialPunctuation = String.valueOf(word.charAt(0));
				word = word.substring(1);
			}
				
			if(endsWithPuntuation(word)) {
				finalPunctuation = String.valueOf(word.charAt(word.length() - 1));
				word = word.substring(0, word.length()-1);
			}
			word = translateNormalWord(word, initialPunctuation, finalPunctuation);
		}
		
		if(wasUpperCase) {
			return word.toUpperCase();
		}else if(wasTitleCase){
			return toTitleCase(word);
		}else if ((wasLowerCase && startsWithPuntuation(word) || endsWithPuntuation(word) || minusWords.length > 1) || wasLowerCase || "nil".equals(word)){
			return word;
		}else {
			throw new TranslatorException();
		}
	}

	private String translateNormalWord(String word, String initialPunctuation, String finalPunctuation) {
		if(phraseStartsWithVowel(word)) {
			if(phraseEndsWithVowel(word)) {
				return initialPunctuation + word + "yay" + finalPunctuation;
			}else if(word.endsWith("y")) {
				return initialPunctuation + word + "nay" + finalPunctuation;
			}else {
				return initialPunctuation + word + "ay" + finalPunctuation;
			}
		}else if("".equals(word)){
			return NIL;
		}else{
			return moveConsonants(word, initialPunctuation, finalPunctuation);
		}
	}

	private String moveConsonants(String word, String initialPunctuation, String finalPunctuation) {
		char firstChar = word.charAt(0);
		char secondChar = word.charAt(1);
		word = word + firstChar;
		if(isConsonant(secondChar)) {
			word = word + secondChar + "ay";
			word = word.substring(2);
			return initialPunctuation + word + finalPunctuation;
		}
		word = word + "ay";
		
		return initialPunctuation + word.substring(1) + finalPunctuation;
	}
	
	private String translateMinusWord(String[] minusWords) throws TranslatorException {
		String wordTemp = "";
		for(int i = 0; i < minusWords.length; i++) {
			wordTemp = wordTemp + translateWord(minusWords[i]);
			if(i+1 != minusWords.length) {
				wordTemp = wordTemp + '-';
			}
		}
		return wordTemp;
	}
	
	private String toTitleCase(String word) {
		String firstCapitalized = word.substring(0, 1).toUpperCase();
		return firstCapitalized + word.substring(1);
	}
	
	private boolean isUpperCase(String word) {
		char[] wordChars = word.toCharArray();
		if("".equals(word)) {
			return false;
		}
		for(int i = 0; i < word.length(); i++) {
			if(!Character.isUpperCase(wordChars[i])) {
				return false;
			}
		}
		return true;
	}
	
	private boolean isLowerCase(String word) {
		char[] wordChars = word.toCharArray();
		for(int i = 0; i < word.length(); i++) {
			if(!Character.isLowerCase(wordChars[i])) {
				return false;
			}
		}
		return true;
	}
	
	private boolean isTitleCase(String word) {
		if(startsWithPuntuation(word)) {
			word = word.substring(1);
		} else if (endsWithPuntuation(word)) {
			word = word.substring(0, word.length() - 1);
		}
		char[] wordChars = word.toCharArray();
		if(wordChars.length > 1) {
			if(!Character.isUpperCase(wordChars[0])) {
			return false;
		}else {
			for(int i = 1; i < wordChars.length; i++) {
				if(!Character.isLowerCase(wordChars[i])) {
					return false;
				}
			}
		}
		return true;
		}
		return false;
	}

	private boolean endsWithPuntuation(String word) {
		return word.endsWith(".") || word.endsWith(",") || word.endsWith(";") || word.endsWith(":") || word.endsWith("?") || word.endsWith("!") || word.endsWith("'") || word.endsWith("(") || word.endsWith(")");
	}

	private boolean startsWithPuntuation(String word) {
		return word.startsWith(".") || word.startsWith(",") || word.startsWith(";") || word.startsWith(":") || word.startsWith("?") || word.startsWith("!") || word.startsWith("'") || word.startsWith("(") || word.startsWith(")");
	}

	private boolean isConsonant(char secondChar) {
		return secondChar != 'a' && secondChar != 'e' && secondChar != 'i' && secondChar != 'o' && secondChar != 'u';
	}
	
	private boolean phraseStartsWithVowel(String word) {
		return (word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o") || word.startsWith("u"));
	}
	
	private boolean phraseEndsWithVowel(String word) {
		return (word.endsWith("a") || word.endsWith("e") || word.endsWith("i") || word.endsWith("o") || word.endsWith("u"));
	}
}
