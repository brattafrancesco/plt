package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void getPhraseShouldReturnInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void translateShouldReturnNil() throws Exception{
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	public void translatePhraseStartingWithAAndEndingWithY() throws Exception{
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void translatePhraseStartingWithUAndEndingWithY() throws Exception{
		String inputPhrase = "uny";
		Translator translator = new Translator(inputPhrase);
		assertEquals("unynay", translator.translate());
	}
	
	@Test
	public void translatePhraseStartingWithVowelAndEndingWithVowel() throws Exception{
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void translatePhraseStartingWithVowelAndEndingWithConsonant() throws Exception{
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void translateHello() throws Exception{
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void translateYellow() throws Exception{
		String inputPhrase = "yellow";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellowyay", translator.translate());
	}

	@Test
	public void translatePhraseStartingWithTwoConsonats() throws Exception{
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void translatePhraseWithMoreWordsWithSpaces() throws Exception{
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
	}
	
	@Test
	public void translatePhraseWithMoreWordsWithNoSpaces() throws Exception{
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
	}
	
	@Test
	public void translatePhraseWithCommaExclamation() throws Exception{
		String inputPhrase = "hello, world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay, orldway!", translator.translate());
	}
	
	@Test
	public void translatePhraseWithCommaExclamationQuestion() throws Exception{
		String inputPhrase = "hello, world! how are you?";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay, orldway! owhay areyay ouyay?", translator.translate());
	}
	
	@Test
	public void translatePhraseWithUpperCase() throws Exception{
		String inputPhrase = "APPLE";
		Translator translator = new Translator(inputPhrase);
		assertEquals("APPLEYAY", translator.translate());
	}
	
	@Test
	public void translatePhraseWithTitleCase() throws Exception{
		String inputPhrase = "Hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("Ellohay", translator.translate());
	}
	
	@Test
	public void translateGenericPhrase() throws Exception{
		String inputPhrase = "Hello, my name is Giorgio! How are YOU?";
		Translator translator = new Translator(inputPhrase);
		assertEquals("Ellohay, myay amenay isay Iorgiogay! Owhay areyay ouyay?", translator.translate());
	}
}
